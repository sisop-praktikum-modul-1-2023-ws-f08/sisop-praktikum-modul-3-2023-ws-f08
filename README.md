

# sisop-praktikum-modul-3-2023-WS-F08

# Kelompok F08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Faiz Haq Noviandra | 5025211132 |
| Nayya Kamila Putri Y | 5025211183 |
| Cavel Ferrari | 5025211198 |

### No 1

# lossless.c

Mendefinisikan struktur Node yang digunakan untuk membangun pohon Huffman
```
struct Node {
  
    char data;
    unsigned freq;
    struct Node *left;
    struct Node *right;
};
```

Menghitung frekuensi karakter dalam file
```
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {

    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        exit(1);
    }

    int c;
    while ((c = fgetc(file)) != EOF) {
        freq[c]++;
    }

    fclose(file);
}
```

Membuat node baru dalam pohon huffman
```
struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}
```
Membuat pohon Huffman berdasarkan frekuensi karakter
```
struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node *nodes[MAX_CHAR];
    int i, j;
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }
    int size = j;
    while (size > 1) {
        struct Node *left = nodes[size - 2];
        struct Node *right = nodes[size - 1];
        struct Node *merged = mergeNode(left, right);
        nodes[size - 2] = merged;
        size--;
    }

    return nodes[0];
}
```

Menghasilkan kode Huffman untuk setiap karakter
```
void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        code[depth] = '\0';
        huffCode[root->data] = strdup(code);
    }

    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);

    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);

    if (depth > 0) {
        free(huffCode[root->data]);
    }
}
```
Menyimpan struktur pohon Huffman ke dalam file
```
void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data, file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}
```

Melakukan kompresi file menggunakan kode Huffman
```
void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        char* code = huffCode[c];
        fwrite(code, sizeof(char), strlen(code), output_file);

    }
    fclose(input_file);
    fclose(output_file);
}
```

Fungsi main sebagai entry point program, melakukan kompresi file dengan algoritma Huffman. 
```
int main() {
    const char* input_filename = "file.txt";
    const char* compressed_filename = "compressed_file.txt";

    unsigned int freq[MAX_CHAR] = {0};
    count_freq(input_filename, freq);

    struct Node* root = create_huffman(freq);

    int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        exit(1);
    }

    pid_t pid = fork();

    if (pid < 0) {
        exit(1);
    } else if (pid > 0) {
        close(pipe_fd[1]);
        unsigned int child_freq[MAX_CHAR];
        read(pipe_fd[0], child_freq, sizeof(child_freq));
        unsigned long long int original_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += freq[i] * sizeof(char) * 8;
        }

        char* huffCode[MAX_CHAR] = {NULL};
        char code[MAX_CHAR];
        generate_code(root, code, 0, huffCode);

        FILE* compressed_file = fopen(compressed_filename, "wb");
        if (compressed_file == NULL) {
            exit(1);
        }
        save_huffman(root, compressed_file);
        fclose(compressed_file);
        compress_file(input_filename, compressed_filename, huffCode);

        unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }

        printf("total original file: %llu\n", original_bits);
        printf("total compressed file: %llu\n", compressed_bits);

        wait(NULL);

    } else {
        close(pipe_fd[0]);
        write(pipe_fd[1], freq, sizeof(freq));
        exit(0);
    }

    return 0;
}
```
Kode tersebut mengimplementasikan algoritma kompresi Huffman dengan menggunakan struktur data pohon Huffman untuk menghasilkan kode Huffman yang unik untuk setiap karakter dalam file. Proses kompresi dilakukan dengan menggantikan setiap karakter dalam file input dengan kode Huffman yang sesuai, sehingga mengurangi ukuran file.


### No 2

# kalian.c
```

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>

// baris a, kolom a
#define BA 4
#define KA 2
// baris b, kolom b 
#define BB 2
#define KB 5

```
Pada baris ini, kita mengimport beberapa library dan mendefinisikan beberapa konstanta yang akan digunakan pada program.

```

int** generateMatrix(int baris, int kolom, int randup, int randlow) {
    int** matriks = malloc(baris * sizeof(int*));
    srand(time(NULL));
    for (int i=0; i<baris; i++) {
        matriks[i] = malloc(kolom * sizeof(int));
        for (int j=0; j<kolom; j++) {
            matriks[i][j] = (rand() % (randup - randlow + 1)) + randlow;
        }
    }
    return matriks;
}

```
Fungsi generateMatrix digunakan untuk membuat matriks dengan ukuran barisxkolom dengan nilai random dari randlow hingga randup. Fungsi ini mengembalikan pointer ke array dua dimensi yang merepresentasikan matriks tersebut.

```

int main()
{
    int **a = generateMatrix(BA, KA, 5, 1);
    int **b = generateMatrix(BB, KB, 4, 1);

    printf("matriks pertama: \n");
    for(int i=0; i<BA; i++){
        for(int j=0; j<KA; j++){
            printf("%d\t", a[i][j]);
        }
        printf("\n");
    }

    printf("\nmatriks kedua: \n");
    for(int i=0; i<BB; i++){
        for(int j=0; j<KB; j++){
            printf("%d\t", b[i][j]);
        }
        printf("\n");
    }

```
Pada main(), kita memanggil fungsi generateMatrix untuk membuat matriks a dan b. Kemudian, kita menampilkan matriks a dan b ke layar.

```




    printf("\nhasil kali kedua matriks: \n");
    int res[BA][KB];
    for(int i=0; i<BA; i++){         //kurang dari baris matriks a
        for(int j=0; j<KB; j++){     //kurang dari kolom matriks b
            res[i][j] = 0;
            for(int k=0; k<BB; k++){ //kurang dari baris dari matriks b
                res[i][j] += a[i][k]*b[k][j];
            }
        }
    }

```
Selanjutnya, kita menghitung hasil perkalian kedua matriks a dan b, dan menyimpan hasilnya dalam array dua dimensi res.

```
    //shared memory
    key_t key = 1234;   //key shared memory yang akan dipakai
    int shmid = shmget(key, sizeof(int)*BA*KB, IPC_CREAT | 0666);   //id sharedmemory yg akan dipakai
    int *shmaddr = (int *)shmat(shmid, NULL, 0);    //variabel buat simpan isi perkalian matriks, sama aja kyk array satu dimensi

    // copy hasil operasi matriks ke shared memory
    int count = 0;
    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
            *(shmaddr + count) = res[i][j];
            count++;
        }
    }

    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
            printf("%d\t", res[i][j]);
        }
        printf("\n");
    }

    sleep(20); //kasih waktu buat run program cinta.c
    
    //detach lalu destroy shared memory
    shmdt((void *)shmaddr);
    shmctl(shmid, IPC_RMID, NULL);
    return 0;
}

```

# cinta.c

```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>

#define BA 4
#define KB 5

```
Baris 1-7: mengimport library yang diperlukan.
Baris 9-10: mendefinisikan nilai tetap untuk ukuran matriks.
```
int kali(int num, int res[], int ressz)
{
    int carry = 0;

    for(int i=0; i<ressz; i++) {      // mengalikan setiap elemen array res[] dengan num (pakai bantuan produk dan carry)
        int produk = (res[i]*num) + carry;
        res[i] = produk%10;              // menyimpan digit terakhir dari produk ke array res[]
        carry = produk/10;     
    }

    while(carry) {                      // memasukkan carry ke array res[] lalu menambah size array res[]
        res[ressz] = carry%10;
        carry/=10;
        ressz++;
    }
    return ressz;
}

```
Baris 13-30: Fungsi kali() mengalikan setiap elemen array res[] dengan num dan mengembalikan ukuran array hasil perkalian.

```
void *fakt(void *args)      //fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
    int res[100];           // maksimal output 100 digit (meskipun worstcase 48 digit)
    
    // ambil value dari argumen
    int n;
    n = *((int *)args);

    res[0] = 1;
    int ressz = 1;

    // melakukan faktorial
    for(int num = 2; num <= n; num++){
        ressz = kali(num, res, ressz);
    }

    // print hasil faktorial
    for(int i = ressz - 1; i >= 0; i--){
        printf("%d", res[i]);
    }
    return NULL;
}

```
Baris 32-47: Fungsi fakt() menghitung nilai faktorial dari argumen yang diberikan dengan bantuan fungsi kali() dan mencetak hasilnya ke layar

```
int main()
{
    //buat menghitung execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int matriks[BA][KB];
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int)*BA*KB, IPC_CREAT | 0666);
    int *shmaddr = (int *)shmat(shmid, NULL, 0);

    printf("hasil kali kedua matriks dari shared memory: \n");
    int count = 0;
    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
            printf("%d\t", *(shmaddr + count));
            matriks[i][j]=*(shmaddr + count); //assign ke matriks buat dipake itung faktorial
            count++;
        }
        printf("\n");
    }

```
Baris 50-68: Fungsi main() menghitung nilai faktorial dari setiap elemen di matriks yang diambil.

```
 //multithreading faktorial
    printf("\nBerikut ini adalah hasil faktorial (THREAD) perkalian matriks sebelumnya: \n");
    pthread_t tid[BA][KB];
    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
            pthread_create(&tid[i][j], NULL, fakt, &matriks[i][j]);
            pthread_join(tid[i][j], NULL);
            printf(" ");
        }
    }
    printf("\n");

    shmdt((void *)shmaddr);

    //perhitungan waktu eksekusi telah selesai
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);

    return 0;
}

```

# sisop.c
```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>

#define BA 4
#define KB 5
 
```
Mengimpor library yang diperlukan untuk program, yaitu: stdio.h untuk fungsi input/output, stdlib.h untuk fungsi umum, time.h untuk fungsi waktu, sys/ipc.h dan sys/shm.h untuk menggunakan shared memory, pthread.h untuk multithreading, dan unistd.h untuk fungsi umum lainnya. Kemudian, melakukan definisi nilai tetap BA dan KB untuk ukuran matriks.

```
int kali(int num, int res[], int ressz)
{
	int carry = 0;

	for(int i=0; i<ressz; i++) {    	//mengalikan setiap elemen array res[] dengan num (pakai bantuan produk dan carry)
		int produk = (res[i]*num) + carry;
		res[i] = produk%10;             //menyimpan digit terakhir dari produk ke array res[]
		carry = produk/10;     
	}

	while(carry) {                      //memasukkan carry ke array res[] lalu menambah size array res[]
		res[ressz] = carry%10;
		carry/=10;
		ressz++;
	}
	return ressz;
}

```
Fungsi kali() adalah fungsi yang digunakan untuk mengalikan suatu bilangan dengan array of integers. Fungsi ini menerima input sebuah bilangan bulat (num), sebuah array of integers (res), dan ukuran array tersebut (ressz). Fungsi ini mengalikan setiap elemen array res dengan num, dimulai dari indeks 0 hingga ressz-1. Setiap hasil perkalian dimasukkan ke dalam res, sedangkan carry-nya dimasukkan ke dalam variabel carry. Selanjutnya, jika carry masih bernilai non-nol, maka carry juga dimasukkan ke dalam array res dan ressz ditambah 1. Fungsi ini mengembalikan nilai ressz yang merupakan ukuran array res setelah mengalikan dengan num.
```
void fakt(int n)      //fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	int res[100];           //maksimal output 100 digit (meskipun worstcase 48 digit)

	res[0] = 1;
	int ressz = 1;

    //melakukan faktorial
	for(int num = 2; num <= n; num++){
        ressz = kali(num, res, ressz);
    }

    //print hasil faktorial
	for(int i = ressz - 1; i >= 0; i--){
        printf("%d", res[i]);
    }
}

```
Fungsi fakt() adalah fungsi yang digunakan untuk menghitung nilai faktorial dari sebuah bilangan. Fungsi ini menerima input sebuah bilangan bulat (n). Fungsi ini menggunakan fungsi kali() untuk mengalikan setiap bilangan dari 2 hingga n dengan sebuah array of integers (res) yang awalnya berisi angka 1. Setelah mengalikan, fungsi ini mencetak hasil.
```
int main()
{
    //buat menghitung execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int matriks[BA][KB];
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int)*BA*KB, IPC_CREAT | 0666);
    int *shmaddr = (int *)shmat(shmid, NULL, 0);

    printf("hasil kali kedua matriks dari shared memory: \n");
    int count = 0;
    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
            printf("%d\t", *(shmaddr + count));
            matriks[i][j]=*(shmaddr + count); //assign ke matriks buat dipake itung faktorial
            count++;
        }
        printf("\n");
    }

    //faktorial tanpa multithread
    printf("\nBerikut ini adalah hasil faktorial (NO-THREAD) perkalian matriks sebelumnya: \n");
    for(int i=0; i<BA; i++){
        for(int j=0; j<KB; j++){
			fakt(matriks[i][j]);
            printf(" ");
        }
    }
    printf("\n");

    shmdt((void *)shmaddr);

    //perhitungan waktu eksekusi telah selesai
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);
    
    return 0;
}

```
Kode ini tidak menggunakan multithreading sehingga perhitungan faktorial dilakukan secara serial. Jika program ini dijalankan dengan matriks yang lebih besar, maka waktu eksekusinya akan semakin lama. 



### No 3

# stream.c

```
void decryptPlaylist() {
    FILE* input_file = fopen("song-playlist.json", "r");
    FILE* output_file = fopen("playlist.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Error opening files");
        exit(1);
    }

    char line[MAX_SIZE];
    while (fgets(line, MAX_SIZE, input_file) != NULL) {
      
        if (strstr(line, "\"method\": \"rot13\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 4);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; 
            for (int i = 0; i < strlen(song); i++) {
                if (isalpha(song[i])) {
                    if ((song[i] >= 'a' && song[i] <= 'm') || (song[i] >= 'A' && song[i] <= 'M')) {
                        song[i] += 13;
                    } else {
                        song[i] -= 13;
                    }
                }
            }
            fprintf(output_file, "%s\n", song);
            free(song);
        }
       
        else if (strstr(line, "\"method\": \"base64\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; 
            unsigned char base64_decoded[MAX_SIZE];
            base64Decode((const unsigned char*)song, strlen((const char*)song), base64_decoded);
            fprintf(output_file, "%s\n", base64_decoded);
            free(song);
        }
        else if (strstr(line, "\"method\": \"hex\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; 
            unsigned char hex_decoded[MAX_SIZE];
            hexDecode((const unsigned char*)song, strlen((const char*)song), hex_decoded);
            fprintf(output_file, "%s\n", hex_decoded);
            free(song);
        }
    }

    fclose(input_file);
    fclose(output_file);

    system("sort -o playlist.txt playlist.txt");

    printf("Playlist decrypted successfully!\n");
}

```
membaca file playlist yang telah dienkripsi dengan teknik rot13, base64, atau hex dan kemudian mendekripsinya. Fungsi ini akan menulis playlist yang telah didekripsi ke dalam file playlist.txt. Fungsi ini juga akan mengurutkan lagu-lagu dalam file playlist.txt sesuai abjad.
```
void playSong(char* song, pid_t user_id) {
    FILE* playlist_file = fopen("playlist.txt", "r");

    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    char line[MAX_LINE_LENGTH];
    int count = 0;
    char found_songs[MAX_SIZE][MAX_LINE_LENGTH];

    while (fgets(line, sizeof(line), playlist_file) != NULL) {
        int len = strlen(line);
        while (len > 0 && isspace(line[len - 1])) {
            line[--len] = '\0';
        }
        if (strcasestr(line, song) != NULL) {
            strncpy(found_songs[count], line, MAX_LINE_LENGTH - 1);
            found_songs[count][MAX_LINE_LENGTH - 1] = '\0';
            count++;
            if (count >= MAX_SIZE) {
                break; 
            }
        }
    }

    fclose(playlist_file);

    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song);
    } else if (count == 1) {
        printf("USER <%ld> PLAYING \"%s\"\n", (long) user_id, found_songs[0]);
    } else {
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\":\n", count, song);
        for (int i = 0; i < count; i++) {
            printf("%d. %s\n", i+1, found_songs[i]);
        }
    }
}

```
memainkan lagu dari file playlist.txt. Fungsi ini akan membaca file playlist.txt dan mencari lagu yang mengandung kata kunci tertentu. Jika ditemukan lebih dari satu lagu yang sesuai dengan kata kunci, maka program akan menampilkan daftar lagu-lagu yang ditemukan dan meminta pengguna untuk memilih salah satu. Setelah pengguna memilih lagu, program akan memainkan lagu tersebut.

### No 4

# Unzip.c
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

```
Baris ini mengimpor pustaka standar untuk I/O, proses, dan tipe data.
```

int main()
{
    int status;
    pid_t pid1, pid2;

```

Fungsi main() akan dijalankan saat program dijalankan. Di sini, kita mendeklarasikan tiga variabel: status bertipe int untuk menyimpan status keluaran dari suatu proses, dan pid1 dan pid2 bertipe pid_t untuk menyimpan ID proses yang dibuat.


```
    // download file
    pid1 = fork();
    if (pid1 == 0)
    {
        printf("Sedang melakukan download...\n");
        char *wget_args[] = {"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execvp("/bin/wget", wget_args);
        exit(0);
    }
    else
    {
        wait(&status);
        if (status != 0)
        {
            printf("Gagal download\n");
            exit(1);
        }
    }

```
Proses pertama adalah mendownload file menggunakan perintah wget. Dalam blok if (pid1 == 0), kita membuat proses anak dengan menggunakan fork(). Jika pid1 bernilai 0, itu berarti kita berada di dalam proses anak. Dalam proses anak ini, kita mencetak pesan "Sedang melakukan download...", kemudian kita menjalankan wget dengan argumen yang diatur dalam array wget_args menggunakan execvp(). Jika execvp() berhasil dieksekusi, baris kode setelahnya tidak akan dieksekusi. Oleh karena itu, kita menambahkan perintah exit(0) untuk keluar dari proses anak dengan kode keluaran 0.
Jika pid1 tidak sama dengan 0, itu berarti kita berada di dalam proses induk. Di dalam blok else, kita menunggu proses anak selesai melakukan download menggunakan wait(&status). Jika status keluaran dari proses anak tidak sama dengan 0, itu berarti download gagal, dan program akan mencetak pesan "Gagal download" dan keluar dengan kode keluaran 1.
```
    // unzip file
    pid2 = fork();
    if (pid2 == 0)
    {
        printf("\n");
        printf("Sedang melakukan unzip file...\n");
        char *zip_args[] = {"/bin/unzip", "-q", "hehe.zip", NULL};
        execvp("/bin/unzip", zip_args);
        exit(0);
    }
    else
    {
        wait(&status);
        if (status != 0)
        {
            printf("Gagal unzip\n");
            exit(1);
        }
        printf("Unzip file selesai\n");
    }

```
Proses kedua adalah melakukan unzip pada file yang sudah didownload. Kita membuat proses anak baru dengan menggunakan fork() dalam blok if (pid2 == 0). Di dalam proses anak ini, kita mencetak pesan "

# Categorize.c

```

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
//For error codes
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
//For threads
#include <pthread.h>
//To access folder contents
#include <dirent.h>
//For nanosleep & time for log
#include <time.h>

#define MAX_LEN 50
#define CATNAME "categorized"
//Amount of storage folders (amount of extentions + "other")
#define folders 8
//Location of log
#define log_path "./log.txt"

//Debug test
#define NORMAL_COLOR  "\x1B[0m"
#define GREEN  "\x1B[32m"
#define BLUE  "\x1B[34m"
#define RED "\x1B[31m"
#define YELLOW "\x1B[33m"
#define MAGENTA "\x1B[36m"

//Initializes MutEx
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3 = PTHREAD_MUTEX_INITIALIZER;

//Init values for sleep

// struct timespec {
//     time_t tv_sec;        /* seconds */
//     long   tv_nsec;       /* nanoseconds */
// };

struct timespec tenth, test = {0, 999};


//Struct to store amount of files in each folder
struct folderFiles{
    char name[10];
    int amount;
    int foldAmount;
};

//Global structs so every thread can access it
struct folderFiles fileAmount[folders];
int max; //Max files per folder

//Struct to dynamically store and add t_id for each folder
typedef struct tidLinked{
    pthread_t t_id;
    struct tidLinked *next;
}tidL;

void init(tidL *list){
    list->next = NULL;
    list->t_id;
}

pthread_t * add(tidL *cur){
    tidL *new_node = (tidL*)calloc(1, sizeof(tidL));

    cur->next = new_node;
    new_node->next = NULL;
    return &(new_node->t_id);
}

void clean_up(tidL *list){
    tidL *temp = NULL;

    while(list != NULL){
        temp = list;
        list = list->next;
        pthread_join(temp->t_id, NULL);
        //free(temp);
    }
}
//Function to convert string to lowercase
void to_lower_case(char * in){
    for(int i = 0; in[i]; i++){
        in[i] = tolower(in[i]);
    }
}
//Function to write log

void logging(int type, char arg1[], char arg2[], char arg3[]){
    pthread_mutex_lock(&mutex1);
    FILE * log = fopen(log_path, "a");

    char log_msg[500];
    char time_str[20];
    //Get time
    time_t rawtime;   
    time ( &rawtime );
    struct tm *timeinfo = localtime ( &rawtime );
    strftime(time_str, 20, "%d-%m-%Y %T", timeinfo);

    if(type == 1){
        //Accessed
        fprintf(log, "%s ACCESSED %s\n", time_str, (arg1 + 2));
    }
    else if(type == 2){
        //Moved
        fprintf(log, "%s MOVED %s file : %.*s > %.*s\n", time_str, arg1, (int) strcspn((arg2+3), "\'"),(arg2 + 3), (int) strcspn((arg3 + 3), "\""), (arg3 + 3));
    }
    else if(type == 3){
        //Made
        fprintf(log, "%s MADE %s\n", time_str, arg1);
    }
    
    pthread_mutex_unlock(&mutex1);
    fclose(log);
}

//Function to create new dir in running dir
/*
Args :
- 
*/
void createDir(char name[]){
    int check = mkdir(name, 0777);

    if (!check){
        printf("Directory %s created\n", name);
        logging(3, name, NULL, NULL);
    }
    else{
        printf("ERROR : \"%s\"", name);
        exit(1);
    }
}
//Function to sort ascendingly
void swap(struct folderFiles* xp, struct folderFiles* yp)
{
    struct folderFiles temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct folderFiles arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        swap(&arr[min_idx], &arr[i]);
    }
}


//Main function
void *recursive(void *args) {

    char dirPath[200];//Folder relative path
    strcpy(dirPath, (char*)args);

    

    printf("%sSTART > Thread managing \"%s\"\n", YELLOW, dirPath);

    //Initializes linked list to store all subthread's id
    tidL tid_list;
    init(&tid_list);

    DIR * d = opendir(dirPath); // open the path
    if(d==NULL) {
        printf("%sERROR : Unable to open %s\n", RED, dirPath);
        return NULL;
        } // if was not able, return
    struct dirent * dir; // for the directory entries
    char f_name[200];//2 uses
    char next_path[200]; // here I am using sprintf which is safer than strcat
    char command[200];
    while ((dir = readdir(d)) != NULL) // if we were able to read somehting from the directory
    {
        

        if(dir-> d_type != DT_DIR){ // if the type is not directory just print it with blue color

            
            
            sprintf(f_name, "%s", dir->d_name);

            //Retrieves extention (.{extention})
            char * ext; //The extention
            char * token = strtok(f_name, ".");
            ext = token;
            // loop through the string to extract all other tokens
            while( token != NULL ) {
                ext = token;
                token = strtok(NULL, ".");
            }
            char d_path[200]; // here I am using sprintf which is safer than strcat
            char dest_path[200];
            sprintf(d_path, "\'%s/%s\'", dirPath, dir->d_name);
            

            //Moves file depending on it's extension
            int other_ext = 0;
            int x = 0;
            int oth = 0;

            //Compares file with every extention in extensions.txt
            char ext2[50];
            sprintf(ext2, "%s", ext);
            to_lower_case(ext2);
            while(x < folders){
                if(!oth && (strcmp(fileAmount[x].name, "other") == 0)){
                    oth = x;
                    continue;
                }
                if(strcmp(fileAmount[x].name, ext2) == 0 ){
                    other_ext = 1;
                    break;
                }
                x++;
            }
            //Moves file
            if (other_ext)
            {
                //LOCK
                
                //If current folder is full
                if(fileAmount[x].amount > 0 && (fileAmount[x].amount % max) == 0){
                    fileAmount[x].foldAmount += 1;
                    sprintf(dest_path, "%s/%s (%d)", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount);
                    createDir(dest_path);
                }
                //Generates destination path (based on amount of folder)
                if(fileAmount[x].foldAmount > 1 && fileAmount[x].amount > 9){
                    sprintf(dest_path, "\"./%s/%s (%d)/%s\"", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount,dir->d_name);
                }
                else if(fileAmount[x].amount < 10){
                    sprintf(dest_path, "\"./%s/%s/%s\"", CATNAME, fileAmount[x].name, dir->d_name);
                }
                //char * commnd = malloc(100);
                
                
                 
                printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);
                
                sprintf(command, "mv %s %s", d_path, dest_path);

                if(!system(command)){
                    fileAmount[x].amount += 1; 
                    logging(2, ext2, d_path, dest_path);
                }
                //nanosleep(&tenth, &test);

                //free(commnd);
                //UNLOCK
                //
            }
            else{
                //LOCK
                
                sprintf(dest_path, "\'./%s/other/%s\'", CATNAME, dir->d_name);
                
                printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);

                //pthread_mutex_lock(&mutex2);
                //char * command = malloc(100);
                
                sprintf(command, "mv %s %s", d_path, dest_path);

                printf("%s\n", command);
                
                if(!system(command)){
                    fileAmount[oth].amount += 1;
                    logging(2, ext2, d_path, dest_path);
                }
                //free(command);
                //UNLOCK
                //nanosleep(&tenth, &test);

                //pthread_mutex_unlock(&mutex2);
            }
        }
        else
            if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".")!=0 && strcmp(dir->d_name,"..")!=0 ) // if it is a directory
            {
                
                printf("%s %s is creating thread for %s\n",GREEN, dirPath, dir->d_name); // print its name in green
                
                pthread_mutex_lock(&mutex3);
                sprintf(next_path, "%s/%s", dirPath, dir->d_name);

                //printf("CREATE THREAD %s : %d\n", dir->d_name, pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path));// recall with the new path
                pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path);
                logging(1, next_path, NULL, NULL);
                //nanosleep(&tenth, &test);
                //nanosleep(&tenth, &test);
                //sleep(1);
                
                pthread_mutex_unlock(&mutex3);
            }
    }
    printf("%sEXITING > Thread managing \"%s\"\n", YELLOW, dirPath);
    //sleep(1);
    clean_up(&tid_list);

    closedir(d); // finally close the directory

    
    return NULL;
}

int main(){
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);
    pthread_mutex_init(&mutex3, NULL);

    FILE * extentions = fopen("./extensions.txt", "r");
    FILE * maxFile = fopen("./max.txt", "r");
    //Untuk menyimpan jumlah file dalam setiap folder hasil, digunakan struct array.

    int count = 0; //Menghitung index

    char catFol[200];
    strcpy(catFol, CATNAME);
    //Creates all storage folders
    createDir(catFol);
    createDir(strcat(catFol, "/other"));
    strcpy(fileAmount[count].name, "other");
    fileAmount[count].amount = 0;
    fileAmount[count].foldAmount = 1;
    count++;
    strcpy(catFol, CATNAME);
    char buffer[MAX_LEN];
    while (fgets(buffer, MAX_LEN, extentions))
    {
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        strcpy(fileAmount[count].name, buffer);
        fileAmount[count].amount = 0;
        fileAmount[count].foldAmount = 1;
        count++;

        strcat(catFol, "/");
        strcat(catFol, buffer);
        createDir(catFol);
        strcpy(catFol, CATNAME);
    }


    //Stores max file in folder
    fgets(buffer, MAX_LEN, maxFile);
    max = atoi(buffer);
    //Sorts order of extention names 
    selectionSort(fileAmount, folders);

    //Starts sorting process
    pthread_t t_id;
    char target[] = "./files";
    //pthread_create(&t_id, NULL, &run, (void *)test);
    pthread_create(&t_id, NULL, &recursive, (void *)target);

    //pthread_join(t_id, NULL);
    sleep(2);
    pthread_join(t_id, NULL);

    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    pthread_mutex_destroy(&mutex3);

    printf("\n\n");
    for(int k = 0; k < folders; k++){
        printf("%s : %d\n", fileAmount[k].name, fileAmount[k].amount);
    }

    fclose(extentions);
    fclose(maxFile);
    return 0;
}

```

Program ini adalah manajer file yang menyortir file dalam sebuah direktori berdasarkan ekstensi dan menempatkan file ke dalam subdirektori berdasarkan jenis file tersebut. Program ini dapat menangani beberapa direktori dan menjalankan beberapa thread untuk mengelola setiap direktori.
Program ini mencakup beberapa file header dan mendefinisikan beberapa macro pada awal program. Program ini menginisialisasi tiga kunci penguncian mutex, dua di antaranya tidak digunakan dalam kode. Program ini juga menginisialisasi sebuah struct yang disebut struct timespec dengan dua atribut, tv_sec dan tv_nsec.
Program mendefinisikan dua struktur, yaitu struct folderFiles dan tidLinked. Struktur folderFiles memiliki tiga atribut, yaitu nama, jumlah, dan foldAmount, yang menyimpan nama folder, jumlah file dalam folder, dan jumlah folder yang dibuat. Struktur tidLinked adalah linked list yang berisi ID thread untuk setiap subdirektori.
Program mendefinisikan beberapa fungsi: init(), add(), clean_up(), to_lower_case(), logging(), createDir(), swap(), selectionSort(), dan recursive().
Fungsi init() menginisialisasi linked list dengan membuat node baru dengan pointer null. Fungsi add() menambahkan node baru ke linked list dengan membuat node baru dan mengembalikan pointer ke ID thread node. Fungsi clean_up() menghapus linked list dan menunggu setiap thread selesai. Fungsi to_lower_case() mengonversi sebuah string ke huruf kecil. Fungsi logging() menulis pesan log ke file. Fungsi createDir() membuat direktori baru dengan nama tertentu. Fungsi swap() menukar dua elemen dalam sebuah array. Fungsi selectionSort() mengurutkan sebuah array secara menaik menggunakan algoritma selection sort.
Fungsi recursive() adalah fungsi utama yang menjalankan program. Fungsi ini mengambil pointer ke string yang berisi path ke direktori yang harus dikelola. Fungsi ini mencetak pesan ke konsol yang menunjukkan thread telah mulai mengelola direktori tertentu. Fungsi ini menginisialisasi linked list dari subthread dan membuka direktori yang harus dikelola. Kemudian, fungsi melakukan loop melalui setiap item di direktori dan memeriksa apakah item tersebut adalah file atau direktori. Jika itu adalah file, fungsi mendapatkan ekstensi file dan menyortir file ke dalam folder berdasarkan ekstensinya. Jika folder tidak ada, fungsi membuat folder baru. Jika folder sudah ada, fungsi menambahkan jumlah file di dalam folder. Jika item adalah direktori, fungsi secara rekursif memanggil dirinya sendiri dengan path subdirektori sebagai argumen. Setelah meloop semua item di direktori, fungsi menyortir subdirektori berdasarkan nama dan menggabungkan semua subthread. Akhirnya, fungsi mencatat pesan yang menunjukkan bahwa ia telah selesai mengelola direktori.




# logchecker.c

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 256
#define MAX_FOL 100

//Struct to store amount of files in each folder
struct cell{
    char name[10];
    int amount;
};

struct cell folAmount[MAX_FOL];
struct cell extAmount[MAX_FOL];

int getIndex(char * k, struct cell * arr){
    for(int x = 0; x < 100; x++){
        if(strcmp(k, (arr+x)->name) == 0){
            return x;
        }
    }
    return -1;
}

//Function to sort ascendingly
void swap(struct cell* xp, struct cell* yp)
{
    struct cell temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct cell arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        if(min_idx != i)
            swap(&arr[min_idx], &arr[i]);
    }
}

int main(void)
{
    //Variables that stores data
    int acc = 0;
    int countExt = 0;
    int countFold = 0;


    FILE* fp;
    fp = fopen("log.txt", "r");
    if (fp == NULL) {
      perror("Failed: ");
      return 1;
    }

    char fileName[MAX_LEN]; //Filename
    char buffer[MAX_LEN];
    char buffer2[MAX_LEN]; //Used to get dest file path
    while (fgets(buffer, MAX_LEN, fp))
    {
        strcpy(buffer2, buffer);
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        //Splits string
        char * token = strtok(buffer, " ");
        token = strtok(NULL, " ");
        token = strtok(NULL, " "); //Log type
        if(strcmp(token, "ACCESSED") == 0){
            acc++;
        }
        else if(strcmp(token, "MADE") == 0){
            //Adds folder to list (ascending)
            //Reads created folder path
            token = strtok(NULL, " ");
            char * num = strtok(NULL, " ");
            strcpy(fileName, token);
            if(num && strlen(num) > 0){
                
                strcat(fileName, " ");
                strcat(fileName, num);
            }
            
            token = strtok(fileName, "/");
            char * token2 = strtok(NULL, "/");
            if(token2){
                strcpy(fileName, token2);
            }
            strcpy(folAmount[countFold].name, fileName);
            
            //printf("CREATED : %s\n", folAmount[countFold].name);
            countFold++;
        }
        else if(strcmp(token, "MOVED") == 0){
            //Process file extention
            token = strtok(NULL, " "); //File type
            int ext_id, fold_id;
            ext_id = getIndex(token, extAmount);
            if((countExt < MAX_FOL) && (ext_id < 0)){
                //printf("New extention : %s\n", token);
                strcpy(extAmount[countExt].name, token);
                ext_id = countExt;
                countExt++;
                //printf("MOVED %s\n", token);
                strcpy(fileName, token);
            }
            extAmount[ext_id].amount++;

            //Process folder (destination)
            token = strtok(buffer2, ">");
            token = strtok(NULL, ">");
            char * destFol = strtok(token, "/"); 
            destFol = strtok(NULL, "/"); 
            //strcpy(fileName, destFol);
            fold_id = getIndex(destFol, folAmount);
            folAmount[fold_id].amount++;

            //Add to categorized
            folAmount[getIndex("categorized", folAmount)].amount++;
        }

    }

    printf("ACCESSED : %d\n", acc);
    printf("--------------------------------------------\n");
    selectionSort(folAmount, MAX_FOL);
    selectionSort(extAmount, MAX_FOL);
    //Test for stored folders

    printf("SORTED FOLDER + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(folAmount[x].name, "")==0)
            continue;

        printf("%s : %d\n", folAmount[x].name, folAmount[x].amount);
    }
    printf("--------------------------------------------\n");
    // //Test for stored folders
    printf("SORTED EXT + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(extAmount[x].name, "")==0)
            continue;
        printf("%s : %d\n", extAmount[x].name, extAmount[x].amount);
    }

    fclose(fp);
    return 0;
}

```
Kode ini adalah sebuah program dalam bahasa C yang membaca file "log.txt" yang berisi catatan aktivitas dalam sebuah sistem file. Program ini akan menghitung jumlah file yang diakses, jumlah folder yang dibuat, dan jumlah file yang dipindahkan ke dalam folder tertentu. Selain itu, program juga akan mengelompokkan file berdasarkan jenis ekstensinya dan menghitung jumlah setiap jenis ekstensi tersebut.
Program ini menggunakan beberapa fungsi dari library C seperti stdio.h, stdlib.h, unistd.h, string.h, dan ctype.h. Program juga menggunakan beberapa konstanta seperti MAX_LEN dan MAX_FOL.
Program ini menggunakan struct cell yang memiliki dua variabel yaitu name yang bertipe char dan amount yang bertipe integer. Struct ini digunakan untuk menyimpan nama folder/ekstensi dan jumlah file dalam folder/ekstensi tersebut.
Program ini memiliki beberapa fungsi seperti getIndex untuk mencari indeks dari folder/ekstensi tertentu dalam array, swap untuk menukar isi dari dua buah cell pada array, dan selectionSort untuk mengurutkan isi dari array folAmount dan extAmount secara ascending.
Proses utama program dimulai dari membuka file "log.txt" dan membaca setiap baris pada file tersebut menggunakan fgets. Setiap baris kemudian diproses untuk menghitung jumlah file yang diakses, jumlah folder yang dibuat, dan jumlah file yang dipindahkan ke dalam folder tertentu.
Untuk setiap baris yang berisi aktivitas "MADE", program akan memproses nama folder yang dibuat dan menyimpannya ke dalam array folAmount. Sedangkan untuk setiap baris yang berisi aktivitas "MOVED", program akan memproses jenis ekstensi dan folder tujuan dari file yang dipindahkan, dan kemudian menyimpannya ke dalam array extAmount dan folAmount secara berurutan.
Setelah selesai membaca semua baris pada file "log.txt", program akan menampilkan hasil perhitungan jumlah file yang diakses, jumlah folder yang dibuat, dan jumlah file yang dipindahkan ke dalam folder tertentu. Program juga akan menampilkan isi dari array folAmount dan extAmount yang sudah diurutkan secara ascending berdasarkan jumlah file.


